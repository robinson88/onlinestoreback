const  express= require('express');
const User =require('../../models/User/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const ProfileUserRoutes = express.Router();


ProfileUserRoutes.post('/',(req,res)=>{


    const name = req.body.name;
    const email = req.body.email;
    const pass = req.body.pass;
    const phone = req.body.phone;
    const address = req.body.address;
    const postalcode = req.body.postalcode;
    const city= req.body.city;
    // const image = req.body.image;

    bcrypt.hash(pass, saltRounds, function (err, hash) {
        const user= new User()

        user.pass=hash;
        user.name=name;
        user.email=email;
        user.phone=phone;
        user.address=address;
        user.postalcode=postalcode;
        user.city=city;
        // user.image=image

        user.save()
            .then((newDatoUser) => {
                res.json(newDatoUser);
            })
            .catch((error) => {
                res.status(500).send(error);
            })

    });
})


ProfileUserRoutes.post('/login', (req, res) => {

    const email = req.body.email;
    const pass = req.body.pass;


    User.findOne({ email: email })

        .then((usuario) => {
            // console.log(usuario)

            if (usuario) {
                bcrypt.compare(pass, usuario.pass, function (err, result) {
                    if (result) {
                        console.log(result)

                        const accessToken = jwt.sign(
                            { usuarioId: usuario._id, usuarioName: usuario.name },
                            process.env.JWT_SECRET);
                        return res.json({ logged: true, token: accessToken, usuario: usuario });
                    }
                    else {
                        console.log(err)

                        return res.status(404).json({ logged: false })
                    }
                });
            }
            else {
                console.log('no match')
                return res.status(404).json({ logged: false, mensaje: "tercero return" })
            }
        })
        .catch((err) => {
            console.log('no encontrado')
            return res.status(404).json({ logged: false, mensaje: "cuarto return" })
        })
});

ProfileUserRoutes.get('/:id', (req, res) => {
  const id = req.params.id;
  User.find({_id: id}, { __v: 0, createdAt: 0, updatedAt: 0 })
      .then((datoUser) => {
          res.send(datoUser)
      })
      .catch((error) => {
          res.status(500).send(error)
      })
});
module.exports = ProfileUserRoutes;
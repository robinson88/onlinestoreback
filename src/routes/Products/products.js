const express = require('express');
const Product = require('../../models/Products/Products');
let multer = require('multer');
const cloudinary = require('cloudinary').v2;
const fs = require('fs-extra');
const uploadproductRoutes = express.Router();

 const VALID_FILE_TYPES = ['image/png', 'image/jpg', 'image/jpeg'];

const IMAGES_URL_BASE = "/productImages"
cloudinary.config({
    cloud_name: 'dro8tyauo',
    api_key: '446463658639534',
    api_secret: 'yrMSfLw4HLjXx73WoNFdgaawlRA'

});
const fileFilter = (req, file, cb) => {
    console.log(file);
  if (!VALID_FILE_TYPES.includes(file.mimetype)) {
    cb(new Error('Invalid file type'));
  } else {
     cb(null, true);
   }
}

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'src/assets/public')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    },

})
  let upload = multer({ storage: storage, fileFilter: fileFilter })



uploadproductRoutes.post('/', upload.single('image'), (req, res) => {
    const result1 = cloudinary.uploader.upload(req.file.path, (error, result) => {
        console.log(error, result)

        const iduser = req.body.iduser;
        const title = req.body.title.toLowerCase();
        const description = req.body.description;
        const categoria = req.body.categoria;
        const condition = req.body.condition;
        const price = req.body.price;
        const image = result.url;
        // const video = '';
        const state = 1;



        const product = new Product()

        product.iduser = iduser;
        product.title = title;
        product.description = description;
        product.categoria = categoria;
        product.condition = condition;
        product.price = price;
        product.image = image;
        product.state = state

        product.save()
            .then((newDatoProduct) => {
                res.json(newDatoProduct);
                console.log(newDatoProduct)
            })
            .catch((error) => {
                res.status(500).send(error);
            })

         fs.unlink(req.file.path);
    })
})






{uploadproductRoutes.get('/', (req, res) => {
    Product.find({ state: 1 }, { __v: 0, createdAt: 0, updatedAt: 0 }).sort({ "_id": -1 })
        .then((products) => {
            res.send(products)
        })
        .catch((error) => {
            res.status(500).send(error)
        })
});}

uploadproductRoutes.get('/', (req, res) => {
    const categoria = req.query.categoria;
    Product.find({ categoria: categoria }, { __v: 0, createdAt: 0, updatedAt: 0 })
        .then((myProduct2) => {
            res.send(myProduct2)

        })
        .catch((error) => {
            res.status(500).send(error)
        })
});

uploadproductRoutes.get('/:id', (req, res) => {
    const id = req.params.id;
    Product.find({ _id: id }, { __v: 0, createdAt: 0, updatedAt: 0 })
        .then((datoProduct) => {
            res.send(datoProduct)
        })
        .catch((error) => {
            res.status(500).send(error)
        })
});

uploadproductRoutes.get('/myproducts/:id', (req, res) => {
    const id = req.params.id;
    Product.find({ iduser: id, state: 1 }, { __v: 0, createdAt: 0, updatedAt: 0 }).sort({ "_id": -1 })
        .then((datoProduct) => {
            res.send(datoProduct)

        })
        .catch((error) => {
            res.status(500).send(error)
        })
});


uploadproductRoutes.get('/myproduct/:id', (req, res) => {
    const id = req.params.id;
    Product.find({ _id: id }, { __v: 0, createdAt: 0, updatedAt: 0 })
        .then((myProduct) => {
            res.send(myProduct)

        })
        .catch((error) => {
            res.status(500).send(error)
        })
});



uploadproductRoutes.post('/updatestate/:id', (req, res) => {
    let id = req.params.id
    Product.findByIdAndUpdate({ _id: id }, {

        state: "0"
    })
        .then((updatedUser, err) => {
            if (err) {
                res.status(500).send(err)
            }
            else {
                res.send("producto eliminado")

            }
        })
})







uploadproductRoutes.post('/edit/:id', upload.single('image'), (req, res) => {
    const id = req.params.id;
    const result1 = cloudinary.uploader.upload(req.file.path, (error, result) => {

        Product.findByIdAndUpdate({_id: id}, {
       
         title : req.body.title.toLowerCase(),
         description : req.body.description,
         condition : req.body.condition,
         price : req.body.price,
         image : result.url
        })
        .then((updatedUser, err) => {
            if(err)
            {
                res.status(500).send(err)
            }
            else{
                res.send("Imagen actualizada")
                
            }
        })
         fs.unlink(req.file.path);
     })
    });


    upvideo.post('/uploadvideo/:id', upload.single('video'), (req, res) => {
        const id = req.params.id;
        cloudinary.uploader.upload(req.file.path, 
        {resource_type: "video", public_id: "assets",
        overwrite: true},
        (error, result) =>{console.log(result, error)
           
            Product.findByIdAndUpdate({_id: id}, {
                video : result.url
               })
           
                .then((newDatoProduct) => {
                    res.json(newDatoProduct);
                    console.log(newDatoProduct)
                })
                .catch((error) => {
                    res.status(500).send(error);
                })
    
             fs.unlink(req.file.path);
        })
    })
        

module.exports = uploadproductRoutes;
const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const upproduct= new Schema(
    {
        iduser:{ type:String,required:true},
        title : { type:String , required: true },
        description: {type:String , required: true},
        categoria:{type:String , require: true},
        condition : { type : String ,required : true },
        price : { type: String , required : true },
        image : { type : String,required: true},
        // video : { type : String,},
        state : {  type: String, default:"1"}
    },
    {
        timeStamp:true
    }
)
// se crea y exporta el modelo
const Product= mongoose.model('Product',upproduct);
module.exports= Product;
const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

const profileUser= new Schema(
    {
        name : { type:String , required: true },
        email : {type:String , required: true},
        pass:{type:String , require: true},
        phone : { type : String ,required : true },
        address : { type: String , required : true },
        postalcode : { type: String , required: true },
        city : { type : String , required: true },
        // image : { type : String,required: true,default:"https://www.vexels.com/media/users//3/145908/raw/52eabf633ca6414e60a7677b0b917d92.jpg"}
    },
    {
        timeStamp:true
    }
)
// se crea y exporta el modelo
const User= mongoose.model('User',profileUser);
module.exports= User;
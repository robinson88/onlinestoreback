

const cors= require ('cors');
require('dotenv').config();
const express = require('express');
const PORT = process.env.PORT ||  4000;
const server = express();
require('./db.js')
////////////////////////CORS//////////////////////////////
server.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  ///////////////////////////////////////////////////////////
  server.use(express.static('public'));
  server.use(cors());
const ProfileUserRoutes = require('./routes/User/user');
const uploadproductRoutes= require('./routes/Products/products')
 server.use(express.json());
 server.use(express.urlencoded({ extended: false }));
 server.use('/profileUser',ProfileUserRoutes)
server.use('/uploadproduct',uploadproductRoutes)

// server.use('/',(req,res)=>{

 
//     // res.send('servidor funcionando')
// })
server.listen(PORT, ()=>{
    console.log(`Server running in http://localhost:${PORT}`);
})